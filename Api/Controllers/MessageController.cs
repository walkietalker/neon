﻿
using Application.Features.Messages;
using Microsoft.AspNetCore.Mvc;

namespace Api.Controllers;

[Microsoft.AspNetCore.Components.Route("api/messages")]
public sealed class MessageController : BaseController
{
    [HttpPost]
    public async Task<ActionResult> CreateMessage(CreateMessageRequestDto dto,
        CancellationToken cancellationToken)
    {
        await Mediator.Send(new CreateMessageQuery(dto), cancellationToken);
        return NoContent();
    }
}