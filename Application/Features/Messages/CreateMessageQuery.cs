﻿using Application.Interfaces.Infrastructure;
using Domain.Entities;
using MediatR;

namespace Application.Features.Messages;

public sealed record CreateMessageQuery(CreateMessageRequestDto Dto) : IRequest;


file sealed class CreateMessageQueryHanlder : IRequestHandler<CreateMessageQuery>
{
    private readonly IAppDbContext _context;

    public CreateMessageQueryHanlder(IAppDbContext context)
    {
        _context = context;
    }

    public async Task Handle(CreateMessageQuery request, CancellationToken cancellationToken)
    {
        await _context.MessageFromUsers.AddAsync(new MessageFromUser
        {
            FullName = request.Dto.FullName,
            Message = request.Dto.Message,
            TelephoneNumber = request.Dto.TelephoneNumber
        }, cancellationToken);

        await _context.SaveChangesAsync(cancellationToken);
    }
}