﻿namespace Application.Features.Messages;

public class CreateMessageRequestDto
{
    public required string FullName { get; init; }

    public required string Message { get; init; }

    public required string TelephoneNumber { get; init; }
}