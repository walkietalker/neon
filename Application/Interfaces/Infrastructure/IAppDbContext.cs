﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace Application.Interfaces.Infrastructure;

public interface IAppDbContext
{
    public DbSet<MessageFromUser> MessageFromUsers { get; set; }

    public Task<int> SaveChangesAsync(CancellationToken cancellationToken);
}