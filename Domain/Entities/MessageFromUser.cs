﻿namespace Domain.Entities;

public class MessageFromUser
{
    public int Id { get; private set; }
    public string FullName { get; set; } = default!;

    public string Message { get; set; } = default!;

    public string TelephoneNumber { get; set; } = default!;
}