﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure;

public class DbInitializer
{
    public static void Initialize(AppDbContext context)
    {
        context.Database.EnsureCreated();
        context.MessageFromUsers.AddAsync(new MessageFromUser
        {
            FullName = "Богданов Павел Алексеевич",
            Message = "Хочу вывеску",
            TelephoneNumber = "+78005553535"
        });
        context.Database.Migrate();
        context.SaveChangesAsync();
    }
}